Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cython-hidapi
Source: https://github.com/trezor/cython-hidapi

Files:     *
Copyright: 2009, 2022-2023 Alan Ott <alan@signal11.us>
           2010-2014 Gary Bishop <gb@cs.unc.edu>
           2010-2014 Pavol Rusnak <stick@gk2.sk>
           2021-2022 libusb/hidapi Team
License:   GPL-3 or BSD-3-clause or python-hidapi-orig

Files:     debian/*
Copyright: 2014-2015 Richard Ulrich <richi@paraeasy.ch>
           2024-2025 Soren Stoutner <soren@debian.org>
License:   GPL-3+
Comment:
 In addition to the GPL-3+, Soren Stoutner also licenses his contributions under
 the GPL-3, BSD-3-clause, or python-hidapi-orig licenses to facilitate
 upstream contributions.

Files:     debian/docs
Copyright: 2024 Soren Stoutner <soren@debian.org>
License:   GPL-3 or BSD-3-clause or python-hidapi-orig

License:   GPL-3
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, version 3 of the License.
Comment:
 On Debian systems the 'GNU General Public License' version 3 is located in
 '/usr/share/common-licenses/GPL-3'.

License:   GPL-3+
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, either version 3 of the License, or (at your option) any later
 version.
Comment:
 On Debian systems the 'GNU General Public License' version 3 is located in
 '/usr/share/common-licenses/GPL-3'.

License:   BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.
 .
 Neither the name of Signal 11 Software nor the names of its contributors may be
 used to endorse or promote products derived from this software without specific
 prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License:   python-hidapi-orig
 You are free to use cython-hidapi code for any purpose.
