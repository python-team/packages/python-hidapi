python-hidapi (0.14.0.post3-3) unstable; urgency=medium

  * Team upload.
  * Do not generate a spurious dependency on python3-pkg-resources
    (Closes: #1083645)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 15 Jan 2025 20:03:29 +0100

python-hidapi (0.14.0.post3-2) unstable; urgency=medium

  * debian/control:  Remove incorrect "Multi-Arch: same".
  * debian/copyright:  Add 2025 to copyright year for myself.

 -- Soren Stoutner <soren@debian.org>  Wed, 08 Jan 2025 15:38:30 -0700

python-hidapi (0.14.0.post3-1) unstable; urgency=medium

  * New upstream release.
  * Add debian/docs to install the upstream documentation.
  * debian/control:
    - Add myself to uploaders.
    - Remove Richard Ulrich <richi@paraeasy.ch> from uploaders at his direction.
    - Add Depends on libhidapi-hidraw0.
    - Refactor binary package description.
    - Remove "Testsuite: autopkgtest-pkg-python" as there are no upstream tests.
    - Bump standards version to 4.7.0 (no changes needed).
    - Reformat for readability.
  * debian/copyright:
    - Add myself to "debian/*".
    - Correct upstream license information.
    - Reformat for readability.
  * debian/patches:
    - Delete obsolete 0001-Revert-upstream-cd2b287.patch.
  * debian/rules:
    - Add "export DEB_BUILD_MAINT_OPTIONS = hardening=+all"
    - Remove obsolete "export PYBUILD_BUILD_ARGS=--with-system-hidapi".
  * debian/upstream/metadata:
    - Correct PyPI package name.
    - Add security contact email.

 -- Soren Stoutner <soren@debian.org>  Wed, 06 Nov 2024 16:25:39 -0700

python-hidapi (0.14.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.14.0
  * Switch back from cython3-legacy to cython3
  * Add Build-dep on pkgconf

 -- Alexandre Detiste <tchet@debian.org>  Mon, 12 Aug 2024 22:26:38 +0200

python-hidapi (0.9.0.post3-5) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Switch to cython3-legacy.
    Closes: #1056858

  [ Andreas Tille ]
  * Standards-Version: 4.6.2
  * Set upstream metadata fields: Repository.
  * watch file standard 4

 -- Andreas Tille <tille@debian.org>  Wed, 10 Jan 2024 17:17:30 +0100

python-hidapi (0.9.0.post3-4) unstable; urgency=medium

  * Team upload
  * Remove retired uploader
  * Update standards version to 4.6.1, no changes needed

 -- Bastian Germann <bage@debian.org>  Thu, 13 Jul 2023 15:43:28 +0200

python-hidapi (0.9.0.post3-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 12:29:01 -0400

python-hidapi (0.9.0.post3-2) unstable; urgency=medium

  * Revert upstream cd2b287 (closes: #966195).

 -- Tristan Seligmann <mithrandi@debian.org>  Fri, 24 Jul 2020 19:27:55 +0200

python-hidapi (0.9.0.post3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Use debhelper-compat instead of debian/compat.

  [ Tristan Seligmann ]
  * New upstream release.
  * Declare rootless build.
  * Add upstream metadata.
  * Set Maintainer to DPMT.
  * Set python3-hid M-A: same.
  * Add autodep8 tests.
  * Bump Standards-Version to 4.5.0 (no changes).
  * Bump debhelper-compat to 13.
  * Switch to dh-sequence-*.

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 20 Jul 2020 12:01:09 +0200

python-hidapi (0.7.99.post21-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #937809

 -- Sandro Tosi <morph@debian.org>  Mon, 23 Dec 2019 18:22:52 -0500

python-hidapi (0.7.99.post21-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.2 (no changes).

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 11 Dec 2017 10:27:02 +0200

python-hidapi (0.7.99.6-3) unstable; urgency=low

  * Include python3 package
  * Convert to pybuild

 -- Marek Marczykowski-Górecki <marmarek@invisiblethingslab.com>  Tue, 31 Oct 2017 01:27:52 +0100

python-hidapi (0.7.99.6-2) unstable; urgency=low

  * Rename binary package to python-hid; this is the correct name as the
    Python module is named "hid", and also avoids a collision with
    python-hidapi from hidapi-cffi.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 23 Aug 2015 01:01:42 +0200

python-hidapi (0.7.99.6-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git
  * Initial release. (Closes: #762920)

 -- Richard Ulrich <richi@paraeasy.ch>  Thu, 25 Sep 2014 22:34:03 +0200
